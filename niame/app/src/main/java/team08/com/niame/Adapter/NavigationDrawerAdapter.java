package team08.com.niame.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import team08.com.niame.Activity.PlaceInfoActivity;
import team08.com.niame.R;
import team08.com.niame.model.Place;

/**
 * Created by CONG_CANH on 12/8/2017.
 */

public class NavigationDrawerAdapter extends RecyclerView.Adapter<PlaceHolder> {
    private List<Place> mDataList;  // = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public NavigationDrawerAdapter(Context context, List<Place> data) {
        inflater = LayoutInflater.from(context);
        this.mDataList = data;
        this.context = context;
    }

    @Override
    public PlaceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.right_nav_drawer_list_item, parent, false);
        PlaceHolder holder = new PlaceHolder(view, mDataList);
        return holder;
    }

    @Override
    public void onBindViewHolder(final PlaceHolder holder, int position) {
        final Place current = mDataList.get(position);

        //populate the data to the views
        holder.bindPlace(current);

        // click listener on RecyclerView items
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PlaceInfoActivity.class);
                intent.putExtra("place_extra", current);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }


}