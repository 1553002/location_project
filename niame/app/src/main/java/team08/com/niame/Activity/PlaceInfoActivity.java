package team08.com.niame.Activity;

import android.Manifest;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import team08.com.niame.Adapter.PlaceInfoAdapter;
import team08.com.niame.Fragment.GoFragment;
import team08.com.niame.PathFinding.DirectionFinderListener;
import team08.com.niame.PathFinding.FindingPath;
import team08.com.niame.PathFinding.Route;
import team08.com.niame.R;
import team08.com.niame.model.CarPark;
import team08.com.niame.model.Convenience;
import team08.com.niame.model.InfoItem;
import team08.com.niame.model.Place;
import team08.com.niame.model.PlaceCollapsedDetail;
import team08.com.niame.model.PlaceComment;

/**
 * Created by CONG_CANH on 12/7/2017.
 */

public class PlaceInfoActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, RatingDialogListener,
        DirectionFinderListener,GoFragment.CommunicationInterface {
    android.support.v7.widget.Toolbar toolbar;
    Place placeIntent;
    GoogleMap mMap;
    SupportMapFragment mapFragment;
    android.support.v4.app.FragmentManager fm;
    LocationManager mLocationManager;
    Marker marker;
    MarkerOptions markerOptions;
    private List<Marker> listDepaMark = new ArrayList<>();
    private List<Marker> listDestMark = new ArrayList<>();
    private List<Polyline> listPaths = new ArrayList<>();
    String departure;
    String destination;

    final static String HTTP = "http://";
    final static String HTTPS = "https://";

    //Firebase
    FirebaseAuth mAuth;
    String placeIndex = null;
    private View btnSave;

    private RatingBar ratingBar;
    private TextView rating;
    FloatingActionButton newComment;
    View userCommentView;

    float placeRatingValue = 0;
    int numberReview = 0;
    boolean isBtnSaveClick = false;
    boolean isBtnCommentClick = false;
    String category = null;

    PlaceComment placeComment = null;
    String userEmail, userID;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_place_information);

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        markerOptions = new MarkerOptions().position(new LatLng(0, 0)).title("My current location");

        //Lay data tu Parceler duoc gui tu GetPlaceFromFirebase
        Bundle b = getIntent().getExtras();
        placeIntent = (Place)b.getParcelable("place_extra");
        placeRatingValue = placeIntent.getRating();

        //Khoi tao Toolbar
        toolbar = (Toolbar)findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(placeIntent.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        InfoItem address = new InfoItem(placeIntent.getAddress(), R.drawable.ic_place_location);
        InfoItem openTime = new InfoItem(placeIntent.getOpen_time(),R.drawable.ic_time_open);
        List<InfoItem> infoItemList = new ArrayList<>();
        infoItemList.add(address);
        infoItemList.add(openTime);

        if (placeIntent instanceof CarPark)
        {
            category = "carpark";
            InfoItem price = new InfoItem(((CarPark) placeIntent).getPrice(),R.drawable.ic_money);
            InfoItem note = new InfoItem(((CarPark) placeIntent).getNotes(),R.drawable.ic_note);

            infoItemList.add(price);
            infoItemList.add(note);
        }
        else if (placeIntent instanceof Convenience)
        {
            category = "convenience";
            InfoItem phone = new InfoItem(((Convenience) placeIntent).getPhone(),R.drawable.ic_phone);
            InfoItem website = new InfoItem(((Convenience) placeIntent).getWebsite(),R.drawable.ic_website);

            infoItemList.add(phone);
            infoItemList.add(website);
        }
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.list_item);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        PlaceInfoAdapter placeInfoAdapter = new PlaceInfoAdapter(this,infoItemList);
        recyclerView.setAdapter(placeInfoAdapter);
        placeInfoAdapter.setOnItemClickListener(new PlaceInfoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(InfoItem item) {
                if (item.getItem_icon() == R.drawable.ic_phone)
                {
                    if (!item.getContent().equals("NA")) {
                        String phoneNumber = "tel:" + item.getContent();
                        openDialer(phoneNumber);
                    }
                }
                else if (item.getItem_icon() == R.drawable.ic_website)
                {
                    openBrower(item.getContent());
                }
            }
        });

        View btnShare = findViewById(R.id.btn_share);
        btnShare.setOnClickListener(this);

        View btnDirect = (View)findViewById(R.id.btn_direction);
        btnDirect.setOnClickListener(this);

        btnSave = (View)findViewById(R.id.btn_favorite);
        btnSave.setOnClickListener(this);

        placeRatingValue =  placeIntent.getRating();

        ratingBar = (RatingBar)findViewById(R.id.rating_bar);
        ratingBar.setRating((float)placeRatingValue);
        rating = (TextView)findViewById(R.id.rating_text);
        rating.setText(Float.toString(placeRatingValue));

        newComment = (FloatingActionButton)findViewById(R.id.btn_new_comment);
        newComment.setOnClickListener(this);
        //setupWindowAnimations();

        mAuth = FirebaseAuth.getInstance();

        userCommentView = (View)findViewById(R.id.user_cmt_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void addFragment(Fragment fragment) {

        fm = getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.go_frame, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

    private void openBrower(String url) {
        if (!url.startsWith(HTTP) && !url.startsWith(HTTPS))
        {
            url = HTTP + url;
        }

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(Intent.createChooser(intent, "Choose browser"));
    }

    private void setupWindowAnimations() {
        Slide slide = (Slide) TransitionInflater.from(this).inflateTransition(R.transition.activity_slide);
        getWindow().setEnterTransition(slide);
    }


    @Override
    protected void onStart() {
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        super.onStart();

        if (mAuth.getCurrentUser()!=null) {
            set_up_user_detail();   //Thiet lap hien thi thong tin cua user
            set_up_place_listion();
            check_place_saved_state();  //Kiem tra tinh trang dia diem da duoc lưu hay chua
            check_place_rated_state();  //Kiem tra user da binh luan hay chua
        }
    }

    private void set_up_place_listion() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("places")
                .child(category).child(placeIntent.getPlaceID());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                {
                   placeRatingValue = Float.parseFloat(dataSnapshot.child("rating").getValue().toString());
                   numberReview = Integer.parseInt(dataSnapshot.child("numberReviews").getValue().toString());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void check_place_rated_state() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users").child(mAuth.getUid())
                .child("comments").child(placeIntent.getPlaceID());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                {
                    Toast.makeText(PlaceInfoActivity.this,"Da comment",Toast.LENGTH_SHORT).show();
                    get_your_comment_detail(userID);
                }
                else
                {
                    Toast.makeText(PlaceInfoActivity.this,"Chua comment",Toast.LENGTH_SHORT).show();
                    TextView dateComment = (TextView)findViewById(R.id.dateComment);
                    dateComment.setText(getResources().getText(R.string.no_your_comment));
                    placeComment = null;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void get_your_comment_detail(String userID) {
        if (userID != null)
        {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference("comments")
                    .child(category).child(placeIntent.getPlaceID()).child(userID);
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        placeComment = dataSnapshot.getValue(PlaceComment.class);   //Đọc toàn bộ dữ liệu vào object
                        TextView dateComment = (TextView)findViewById(R.id.dateComment);
                        dateComment.setText(placeComment.getCommentDate());
                        updateYourCommentView(placeComment.getRating(), placeComment.getContent());
                    }
                    else
                        placeComment = null;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void check_place_saved_state() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users")
                .child(mAuth.getUid()).child("favorites");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                placeIndex = null;
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    if (child.child("placeID").getValue().toString().equals(placeIntent.getPlaceID())) {
                        placeIndex = child.getKey().toString();
                        break;
                    }
                }
                updateUI(placeIndex);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void set_up_user_detail() {
        //Enable user comment view
        userCommentView.setVisibility(View.VISIBLE);

        //Set up user avatar
        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            Uri imageURL = acct.getPhotoUrl();
            if (imageURL != null) {
                CircleImageView imageView = findViewById(R.id.profile_image);
                Picasso.with(this).load(imageURL).placeholder(R.drawable.person_flat)
                        .into(imageView);
            }
            userEmail = acct.getEmail();
            userID = mAuth.getUid();
        }

        TextView userName = (TextView)findViewById(R.id.userName);
        userName.setText(acct.getDisplayName());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
        {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id)
        {
            case R.id.btn_share:
            {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, placeIntent.getName()+", "+placeIntent.getAddress()); // Simple text and URL to share
                sendIntent.setType("text/plain");
                this.startActivity(sendIntent);
            }break;
            case R.id.btn_direction:
                initaliseMap();
                break;
            case R.id.btn_favorite:
                isBtnSaveClick = true;
                if (mAuth.getCurrentUser()==null)
                {
                    List<AuthUI.IdpConfig> providers = Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build());
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setAvailableProviders(providers)
                                    .build(), 123);
                }
                else {
                    isBtnSaveClick = false;
                    if (placeIndex != null) //Đã saved
                        deletePlaceToFirebase();
                    else
                        savePlaceToFirebase();
                }
                break;
            case R.id.btn_new_comment:
                isBtnCommentClick = true;
                if (mAuth.getCurrentUser()==null)
                {
                    List<AuthUI.IdpConfig> providers = Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build());
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setAvailableProviders(providers)
                                    .build(), 123);
                }
                else {
                    isBtnCommentClick = false;
                    showDialog(placeComment);
                    //showReviewDialog();
                }

                break;
        }
    }

    private void showReviewDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialog_layout = inflater.inflate(R.layout.comment_dialog,null);
        AlertDialog.Builder db = new AlertDialog.Builder(PlaceInfoActivity.this);
        db.setView(dialog_layout);

        db.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        final String[] description = {"Rất tệ", "Không tốt", "Tạm", "Tốt", "Tuyệt vời !!!"};
        final TextView noteDescription = dialog_layout.findViewById(R.id.note_description);
        RatingBar commentRatingBar = dialog_layout.findViewById(R.id.rating_bar);
        db.show();
        commentRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                noteDescription.setText(description[(int) (rating-1)]);
            }
        });
        commentRatingBar.setRating(3);


    }


    private void showDialog(PlaceComment placeComment) {
        AppRatingDialog.Builder db = new AppRatingDialog.Builder();
        db.setPositiveButtonText("Đăng")
                .setNegativeButtonText("Hủy")
                .setNoteDescriptions(Arrays.asList("Rất tệ", "Không tốt", "Tạm", "Tốt", "Tuyệt vời !!!"))
                .setDefaultRating(3)
                .setTitle("Xếp hạng và đánh giá")
                .setDescription("Bài đánh giá và xếp hạng của bạn sẽ được đăng công khai.")
                .setStarColor(R.color.nliveo_orange_colorPrimary)
                .setNoteDescriptionTextColor(R.color.blue)
                .setTitleTextColor(R.color.blue)
                .setDescriptionTextColor(R.color.nliveo_divider)
                .setHint("Chia sẽ chi tiết về trải nghiệm của riêng bạn tại địa điểm này ...")
                .setHintTextColor(R.color.blue)
                .setCommentTextColor(R.color.blue)
                .setCommentBackgroundColor(R.color.white)
                .setWindowAnimation(R.style.MyDialogFadeAnimation);

        if (placeComment != null)
        {
            db.setNeutralButtonText("Xóa");
            db.setDefaultRating(placeComment.getRating());
            if (placeComment.getContent().equals("")==false) {
                db.setDefaultComment(placeComment.getContent());
            }
        }
        db.create(this).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == 123) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            // Successfully signed in
            if (resultCode == RESULT_OK) {
                userCommentView.setVisibility(View.VISIBLE);

                setupFirstTimeUse();
                onStart();

                isBtnCommentClick = isBtnSaveClick = false;
                return;
            } else {
                // Sign in failed
                if (response == null) {
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    return;
                }
            }
        }
    }
    private void initaliseMap() {
        fm = getSupportFragmentManager();
        mapFragment = SupportMapFragment.newInstance();
        fm.beginTransaction().replace(R.id.container, mapFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(mapFragment.getClass().getSimpleName())
                .commit();

        mapFragment.getMapAsync(this);
    }

    /*1553002 - 11/Dec/2017*/
    private void openDialer(String phone_number)
    {
        //Tao intent toi Dialer
        Intent intent = new Intent(Intent.ACTION_DIAL);

        //Set phone number cho intent
        intent.setData(Uri.parse(phone_number));

        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
            }
        } catch (Resources.NotFoundException e) {
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //Lay vi tri hien tai
            Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            updateCurrentMarkerF(location);
            addFragment(new GoFragment());
            LatLng start = new LatLng(location.getLatitude(),location.getLongitude());
            StringBuilder stStart = new StringBuilder(start.latitude + "," + start.longitude);
            sendText(stStart.toString(), placeIntent.getAddress());
        }

    }

    private void updateCurrentMarkerF(Location location) {
        //Dam bao location da co gia tri
        if (location == null)
            return;

        if (marker != null)
            marker.remove();
        int height = 150;
        int width = 150;

        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.new_blue_dot);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        LatLng curLocation = new LatLng(location.getLatitude(),location.getLongitude());
        marker = mMap.addMarker(new MarkerOptions()
                .position(curLocation)
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
        );

        mMap.moveCamera(CameraUpdateFactory.newLatLng(curLocation));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(curLocation, 16));
    }

    @Override
    public void onDirectionFinderStart() {
        if (listDepaMark != null) {
            for (Marker marker : listDepaMark) {
                marker.remove();
            }
        }

        if (listDestMark != null) {
            for (Marker marker : listDestMark) {
                marker.remove();
            }
        }

        if (listPaths != null) {
            for (Polyline polyline : listPaths) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        listPaths = new ArrayList<>();
        listDepaMark = new ArrayList<>();
        listDestMark = new ArrayList<>();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
           ((TextView) findViewById(R.id.texttime)).setText(route.estitime.text);
            ((TextView) findViewById(R.id.textdistance)).setText(route.distance.text);
            ((TextView) findViewById(R.id.text_place_destination)).setText(placeIntent.getAddress());
            mMap.clear();

            listDepaMark.add(mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.new_blue_dot))
                    .anchor(0.5f, 0.5f)
                    .draggable(false)
                    .title(route.startAddress)
                    .position(route.startLocation)));

            listDestMark.add(mMap.addMarker(new MarkerOptions()
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.parseColor("#1976D2")).
                    width(16)
                    .zIndex(8);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            listPaths.add(mMap.addPolyline(polylineOptions));
        }
    }

    @Override
    public void sendText(String depa, String dest) {
        departure = depa;
        destination = dest;
        sendRequest();
    }

    private void sendRequest() {
        if (departure.isEmpty() || destination.isEmpty()) {
            Toast.makeText(this, "Diem xuat phat va diem den khong hop le", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            new FindingPath((DirectionFinderListener) this, departure, destination).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }



    private void updateUI(String placeIndex) {
        btnSave = (View) findViewById(R.id.btn_favorite);
        TextView saved_text = (TextView) btnSave.findViewById(R.id.text_favorite);
        ImageView saved_img = (ImageView) btnSave.findViewById(R.id.img_favorite);
        if (placeIndex == null) {
            saved_text.setText("LƯU");
            saved_img.setImageResource(R.drawable.ic_un_favorite);
        }
        else
        {
            saved_text.setText("ĐÃ LƯU");
            saved_img.setImageResource(R.drawable.ic_favorite);
        }
    }

    private void savePlaceToFirebase()
    {
        FirebaseUser curUser = mAuth.getCurrentUser();
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users")
                .child(curUser.getUid()).child("favorites");

        PlaceCollapsedDetail placeCollapsedDetail = new PlaceCollapsedDetail(placeIntent.getPlaceID());

        placeCollapsedDetail.setCategory(category);

        placeIndex = ref.push().getKey();
        ref.child(placeIndex).setValue(placeCollapsedDetail);
    }

    private void deletePlaceToFirebase()
    {
        FirebaseUser curUser = mAuth.getCurrentUser();
        final DatabaseReference ref = FirebaseDatabase.getInstance().getReference("users")
                .child(curUser.getUid()).child("favorites");
        ref.child(placeIndex).removeValue();
    }

    private void setupFirstTimeUse()
    {
        final DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users")
                .child(mAuth.getCurrentUser().getUid());
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){

                }
                else
                {
                    FirebaseDatabase.getInstance().getReference("users").child(mAuth.getCurrentUser().getUid())
                            .child("email").setValue(mAuth.getCurrentUser().getEmail());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onPositiveButtonClicked(int i, String comment) {
        if (placeComment == null)
            addRating(i);
        else    //Da tung comment va chinh sua
        {            int oldRating = placeComment.getRating();
            if (i != oldRating)
            {
                modifyRating(oldRating, i);
            }
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String commentDate = dateFormat.format(new Date());

        DatabaseReference commentRef = FirebaseDatabase.getInstance().getReference("comments")
                .child(category).child(placeIntent.getPlaceID()).child(userID);
        //Tao object comment de luu tru
        placeComment = new PlaceComment(userEmail, commentDate, comment, i);
        commentRef.setValue(placeComment);

        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users").child(mAuth.getUid())
                .child("comments").child(placeIntent.getPlaceID());
        userRef.setValue(true);


    }

    private void modifyRating(int oldRating, int i) {
        DatabaseReference placeRef = FirebaseDatabase.getInstance().getReference().child("places").child(category)
                .child(placeIntent.getPlaceID());

        float oldRatingTotal = placeRatingValue*numberReview;
        float newAvgRating = (oldRatingTotal - oldRating + i)/numberReview;

        placeRef.child("rating").setValue(newAvgRating);
    }

    private void addRating(int i) {
        DatabaseReference placeRef = FirebaseDatabase.getInstance().getReference().child("places").child(category)
                .child(placeIntent.getPlaceID());

        int newNumRatings = numberReview + 1;
        float oldRatingTotal = placeRatingValue*numberReview;
        float newAvgRating = (oldRatingTotal + i)/newNumRatings;

        placeRef.child("numberReviews").setValue(newNumRatings);
        placeRef.child("rating").setValue(newAvgRating);
    }

    private void updateYourCommentView(int i, String comment) {
        RatingBar yourRating = (RatingBar)findViewById(R.id.your_rating_bar);
        yourRating.setRating(i);
        TextView yourContent = (TextView)findViewById(R.id.your_comment_content);
        yourContent.setText(comment);
    }

    @Override
    public void onNegativeButtonClicked() {

    }

    @Override
    public void onNeutralButtonClicked() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        ref.child("comments").child(category).child(placeIntent.getPlaceID()).child(userID).removeValue();
        ref.child("users").child(mAuth.getUid()).child("comments").child(placeIntent.getPlaceID()).removeValue();

        int newNumRatings = numberReview - 1;
        float oldRatingTotal = placeRatingValue*numberReview;
        float newAvgRating;
        if (newNumRatings != 0)
            newAvgRating= (oldRatingTotal - placeComment.getRating())/newNumRatings;
        else
            newAvgRating = 1;

        ref.child("places").child(category).child(placeIntent.getPlaceID()).child("numberRating").setValue(newNumRatings);
        ref.child("places").child(category).child(placeIntent.getPlaceID()).child("rating").setValue(newAvgRating);

        placeComment = null;

        //Thiet lap lai view
        RatingBar yourRating = (RatingBar)findViewById(R.id.your_rating_bar);
        yourRating.setRating(0);
        TextView yourContent = (TextView)findViewById(R.id.your_comment_content);
        yourContent.setText(null);
        TextView dateComment = (TextView)findViewById(R.id.dateComment);
        dateComment.setText(getResources().getText(R.string.no_your_comment));
    }



}
