package team08.com.niame.place;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import team08.com.niame.model.CarPark;
import team08.com.niame.model.Convenience;
import team08.com.niame.model.Place;

/**
 * Created by CONG_CANH on 11/30/2017.
 */

public class AddNewPlaceToFirebase {
    Place newPlace;

    public void add(){
        DatabaseReference placeDetail = FirebaseDatabase.getInstance().getReference("places").child("carpark");
        for (int i=0; i<10; i++)
        {
            placeDetail.push().setValue(new CarPark());
        }
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("places").child("convenience");
        for (int i=0; i<20; i++)
        {
            ref.push().setValue(new Convenience());
        }

        //String placeID = placeDetail.push().getKey();   //Day key moi cho place
        /*CarPark carPark = new CarPark("Bãi xe bệnh viện ĐH Y Dược",
                "215 Hồng Bàng, phường 11, Quận 5, Hồ Chí Minh, Vietnam",
                "6:00AM - 22:00PM",
                10.7554675,
                106.6640843,
                "30,000 VNĐ/lượt",
                "Chỉ giữ xe ban ngày.\nĐậu xe tầng hầm, lối vào cổng Tản Đà, nhưng thường hết chỗ phải đậu ngoài.");

       CarPark[] carparkList = new CarPark[5];

       carparkList[0] = carPark;
       carparkList[1] = new CarPark("Ký túc xá ĐH Bách Khoa",
               "18A Nguyễn Kim, Phường 12, Quận 5, Hồ Chí Minh, Vietnam",
               "NA",
               10.757051,
               106.663175,
               "1,400,000 VNĐ/tháng",
               "Sau CoopMart Lý Thường Kiệt, ngõ xuống hầm đối diện chùa Thiền Quang đường Đào Duy Từ");
        carparkList[2] = new CarPark("Bệnh viện Nhân dân 115",
                "527 Sư Vạn Hạnh, 12, Quận 10, Hồ Chí Minh, Vietnam",
                "Mở cửa 24/7",
                10.775,
                106.6676615,
                "30,000 VNĐ/lượt",
                "Giữ xe không lấy vé");
        carparkList[3] = new CarPark("Bãi xe siêu thị BigC",
                "268 Tô Hiến Thành, Phường 15, Quận 10, Hồ Chí Minh, Vietnam",
                "6:00AM - 22:00PM",
                10.7780699,
                106.665456,
                "30,000 VNĐ/lượt",
                "Đậu khuôn viên trước siêu thị");
        carparkList[4] = new CarPark("Bãi đậu xe ô tô hẻm 283 Cách Mạng Tháng Tám",
                "283 Cách Mạng Tháng Tám, Phường 13, Quận 10, Hồ Chí Minh, Vietnam",
                "24/7",
                10.781248,
                106.675192,
                "NA",
                "Tuyến đường được phép đậu xe tự do vòng quanh công viên, khu dân cư an ninh");*/

       for (int i=0; i<10; i++)
            placeDetail.child("convenience").push().setValue(new Convenience());
    }
}
