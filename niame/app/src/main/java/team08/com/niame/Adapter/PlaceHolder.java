package team08.com.niame.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import team08.com.niame.R;
import team08.com.niame.model.CarPark;
import team08.com.niame.model.Place;

/**
 * Created by CONG_CANH on 12/8/2017.
 */

public class PlaceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    ImageView iconImage; //Icon của địa điểm
    /*    String name;    //Tên của địa điểm
        String address; //Địa chỉ của địa điểm
        double lat, lng; //Tọa độ của địa điểm*/
    private List<Place> placeObject;

    View mView;
    Context mContext;

    public TextView namePlace;
    public TextView addressPlace;
    public RatingBar ratingBar;
    private TextView ratingText;

    public PlaceHolder(View itemView, final List<Place> placeObject) {
        super(itemView);

        //Gan du lieu
        this.placeObject = placeObject;

        mView = itemView;
        mContext = itemView.getContext();

        //Khoi tao text view
        namePlace = (TextView) itemView.findViewById(R.id.title);
        addressPlace = (TextView) itemView.findViewById(R.id.expand_content);
        iconImage = (ImageView) itemView.findViewById(R.id.place_icon);
        ratingBar = (RatingBar)itemView.findViewById(R.id.rating_bar);
        ratingText = (TextView)itemView.findViewById(R.id.rating_text);

        itemView.setOnClickListener(this);
    }

    public void bindPlace(Place mplace) {
        int iconID;
        if (mplace instanceof CarPark)
            iconID = R.drawable.ic_carpark_location;
        else
            iconID = R.drawable.ic_convenience_location;

        iconImage.setImageResource(iconID);
        namePlace.setText(mplace.getName());
        addressPlace.setText(mplace.getAddress());
        ratingText.setText(Float.toString(mplace.getRating()));
        ratingBar.setRating(mplace.getRating());
    }

    @Override
    public void onClick(View v) {
        /*final ArrayList<Place> places = new ArrayList<>();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("places");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren())
                {
                    for (DataSnapshot snapshot_2 : snapshot.getChildren())
                    {
                        places.add(snapshot_2.getValue(Place.class));
                    }
                }
            }

            int itemPosition = getLayoutPosition();
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
    }
}
