package team08.com.niame.model;

/**
 * Created by CongCanh on 12/23/2017.
 */

public class PlaceCollapsedDetail {
    String placeID;
    String category;

    public PlaceCollapsedDetail() {
    }

    public PlaceCollapsedDetail(String placeID, String category) {
        this.placeID = placeID;
        this.category = category;
    }

    public PlaceCollapsedDetail(String placeID) {
        this.placeID = placeID;
    }

    public String getCategory() {
        return category;
    }

    public String getPlaceID() {
        return placeID;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
