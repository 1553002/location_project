package team08.com.niame.Activity;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import team08.com.niame.R;

/**
 * Created by Cong Canh on 12/13/2017.
 */

public class SplashScreen extends AppCompatActivity{

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    Thread splashTread;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_splash);
        StartAnimations();
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.from_top);
        anim.reset();
        ImageView elip = (ImageView) findViewById(R.id.elip);
        elip.clearAnimation();
        elip.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.from_bottom);
        anim.reset();
        ImageView nia = (ImageView) findViewById(R.id.nia);
        nia.clearAnimation();
        nia.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.from_right);
        anim.reset();
        ImageView me = (ImageView) findViewById(R.id.me);
        me.clearAnimation();
        me.startAnimation(anim);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 2400) {
                        sleep(100);
                        waited += 100;
                    }
                    Intent intent = new Intent(SplashScreen.this,
                            HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    SplashScreen.this.finish();
                } catch (InterruptedException e) {
                    // do nothing
                } finally {
                    SplashScreen.this.finish();
                }

            }
        };
        splashTread.start();

    }
}
