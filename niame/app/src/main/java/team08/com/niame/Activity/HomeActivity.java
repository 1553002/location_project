package team08.com.niame.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import team08.com.niame.BuildConfig;
import team08.com.niame.Fragment.MenuFragment;
import team08.com.niame.Fragment.NavigationDrawerFragment;
import team08.com.niame.Geofence.GeofenceContract;
import team08.com.niame.Geofence.GeofenceStorage;
import team08.com.niame.Geofence.GeofenceTransitionsIntentService;
import team08.com.niame.R;
import team08.com.niame.place.AddNewPlaceToFirebase;
import team08.com.niame.place.GetPlaceFromFirebase;

/**
 * Created by CONG_CANH on 12/6/2017.
 */

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, View.OnClickListener,com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    //View
    private Context mContext;
    private FloatingActionButton btnGPS;
    private FloatingActionButton btnExpandedMenu;
    private FloatingActionButton btnMore;
    private CoordinatorLayout mCoordinatorLayout;
    private DrawerLayout mDrawerLayout;

    final String TAG = "HomeActivity";
    final static int PERMISSION_ALL = 1337;
    final static String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION};

    private GoogleMap mMap;
    LocationManager mLocationManager;
    String provider = null;
    Location mCurrentLocation;
    MarkerOptions markerOptions;
    Marker marker;

    Boolean isGeofenceClick = false;

    //geofence var
    private LocationRequest locationRequest;

    // Choose an arbitrary request code value
    private static final int RC_SIGN_IN = 123;
    public static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 100; // 100 m
    private static final int LOCATION_REFRESH_TIME = 2000; //2 seconds
    private static final int LOCATION_REFRESH_DISTANCE = 5; //5 m
    protected ArrayList<LatLng> mGeofenceList;
    private PendingIntent mGeofencePendingIntent;
    private SharedPreferences mSharedPreferences;
    private GoogleApiClient client;

    MenuFragment menuFragment;
    private NavigationView mLeftNav;


    //Firebase
    FirebaseAuth mAuth; //Current author

    @SuppressWarnings({"MissingPermission"})
    @Override
    protected void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();
        if (mLocationManager != null) {
            if (provider!=null) {
                mLocationManager.requestLocationUpdates(provider, LOCATION_REFRESH_TIME, LOCATION_REFRESH_DISTANCE, this);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    protected void onStop() {
        super.onStop();

        //Disconnecting the connection
        client.disconnect();
    }

    //1553002 - Le Cong Canh
    private void updateUI(FirebaseUser currentUser) {
        if (currentUser!=null)
        {
            Uri imageURL = null;
            mLeftNav.removeHeaderView(mLeftNav.getHeaderView(0));
            View header = LayoutInflater.from(this).inflate(R.layout.left_nav_list_header, null);
            mLeftNav.addHeaderView(header);
            mLeftNav.getMenu().findItem(R.id.nav_log_out).setVisible(true);//Show log-out button
            mLeftNav.getMenu().findItem(R.id.nav_log_in).setVisible(false);//Hide log-in button
            mLeftNav.getMenu().findItem(R.id.nav_add_place).setEnabled(true);
            mLeftNav.getMenu().findItem(R.id.nav_favorite_place).setEnabled(true);
            View view = mLeftNav.getHeaderView(0);

            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
            if (acct!=null){
                imageURL = acct.getPhotoUrl();
                if (imageURL!=null)
                {
                    CircleImageView imageView = view.findViewById(R.id.profile_image);
                    Picasso.with(HomeActivity.this).load(imageURL).placeholder(R.drawable.person_flat)
                            .into(imageView);
                }
            }

            TextView email = view.findViewById(R.id.userEmail);
            email.setText(mAuth.getCurrentUser().getEmail());

            TextView userName = view.findViewById(R.id.userName);
            userName.setText(mAuth.getCurrentUser().getDisplayName());

        } else{
            mLeftNav.removeHeaderView(mLeftNav.getHeaderView(0));
            View header = LayoutInflater.from(this).inflate(R.layout.left_nav_header_normal, null);
            mLeftNav.addHeaderView(header);
            mLeftNav.getMenu().findItem(R.id.nav_log_in).setVisible(true);
            mLeftNav.getMenu().findItem(R.id.nav_log_out).setVisible(false);//Hide log-out button
            mLeftNav.getMenu().findItem(R.id.nav_add_place).setEnabled(false);
            mLeftNav.getMenu().findItem(R.id.nav_favorite_place).setEnabled(false);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        mContext = this;

        //Khoi tao ban do
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.google_map);
        mapFragment.getMapAsync(this);

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        markerOptions = new MarkerOptions().position(new LatLng(0, 0)).title("My current location");

        btnGPS = (FloatingActionButton) findViewById(R.id.btn_gps);
        btnGPS.setOnClickListener(this);

        btnExpandedMenu = (FloatingActionButton) findViewById(R.id.btn_menu_expand);
        btnExpandedMenu.setOnClickListener(this);

        btnMore = (FloatingActionButton) findViewById(R.id.btn_list_items);
        btnMore.setOnClickListener(this);

        Button btnHamburger = (Button)findViewById(R.id.btn_hamburger_menu);
        btnHamburger.setOnClickListener(this);

        mCoordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        setUpDrawer();

        mGeofenceList = new ArrayList<>();
        mGeofencePendingIntent = null;
        mSharedPreferences = getSharedPreferences(BuildConfig.APPLICATION_ID + ".SHARED_PREFERENCES_NAME", MODE_PRIVATE);
        bulidGoogleApiClient();

        //Thiet lap fragment autocomplete su dung google place API
        setupPlaceAutoComplete();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !isPermissionGranted()) {
            requestPermissions(PERMISSIONS, PERMISSION_ALL); //Bat buoc phai chap nhan moi yeu cau
        }else
            makeRequestLocation();

        if (!isLocationEnabled())
            showAlert(1);

        if(savedInstanceState!=null){
            isGeofenceClick = savedInstanceState.getBoolean("isGeofenceClick");
        }

        mAuth = FirebaseAuth.getInstance();
    }

    private void bulidGoogleApiClient() {
        client = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        client.connect();
    }

    private void logSecurityException(SecurityException securityException) {
        Log.e(TAG, "Invalid location permission. " +
                "You need to use ACCESS_FINE_LOCATION with geofences", securityException);
    }

    //1553020 - Nguyen Ngoc Nhan
    private void addGeoMarker(String key, LatLng latLng, String snippet_content) {
        mMap.addMarker(new MarkerOptions()
                .title("G:" + key)
                .snippet(snippet_content)
                .position(latLng)).showInfoWindow();

        mMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(GEOFENCE_RADIUS_IN_METERS)
                .strokeColor(Color.RED)
                .fillColor(Color.parseColor("#80ff0000")));
        mGeofenceList.add(latLng);
    }

    //1553020 - Nguyen Ngoc Nhan
    private int getNewGeofenceNumber() {
        int number = mSharedPreferences.getInt(BuildConfig.APPLICATION_ID + ".NEW_GEOFENCE_NUMBER", 0);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(BuildConfig.APPLICATION_ID + ".NEW_GEOFENCE_NUMBER", number + 1);
        editor.commit();
        return number;
    }


    //1553002 - Le Cong Canh
    private void setUpDrawer() {
        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.nav_drwr_fragment);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED); //Lock drawer swipping mode
        drawerFragment.setUpDrawer(R.id.nav_drwr_fragment, mDrawerLayout);

        //Set up the left navigation
        mLeftNav = (NavigationView) findViewById(R.id.nvView);
        mLeftNav.setNavigationItemSelectedListener(navLeftItemSelectedListener);

    }

    NavigationView.OnNavigationItemSelectedListener navLeftItemSelectedListener = new NavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId())
            {
                case R.id.nav_log_in:
                    log_in_google_account();
                    break;
                case R.id.nav_log_out:
                    AuthUI.getInstance().signOut(mContext).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            updateUI(mAuth.getCurrentUser());
                            Snackbar snackbar = Snackbar
                                    .make(mCoordinatorLayout, "Đăng xuất thành công.", Snackbar.LENGTH_LONG);

                            snackbar.show();
                        }
                    });
                    break;
                case R.id.nav_favorite_place:
                    if(mAuth.getCurrentUser()==null)
                    {
                        log_in_google_account();
                    }
                    else {
                        mDrawerLayout.closeDrawer(Gravity.START);
                        mDrawerLayout.openDrawer(Gravity.END);
                        GetPlaceFromFirebase temp = new GetPlaceFromFirebase(mMap, mContext);
                        temp.getAllSavedPlace(mAuth.getUid());
                    }
                    break;
                case R.id.nav_add_place:
                    AddNewPlaceToFirebase addNewPlaceToFirebase = new AddNewPlaceToFirebase();
                    addNewPlaceToFirebase.add();
                    break;
            }
            return false;
        }
    };

    private void log_in_google_account() {
        if (mAuth.getCurrentUser() != null) {
            // already signed in
        } else {
            List<AuthUI.IdpConfig> providers = Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build());
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(providers)
                            .build(), RC_SIGN_IN);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            // Successfully signed in
            if (resultCode == RESULT_OK) {
                Snackbar snackbar = Snackbar.make(mCoordinatorLayout, R.string.login_success, Snackbar.LENGTH_LONG);
                snackbar.show();

                updateUI(mAuth.getCurrentUser());
                setupFirstTimeUse();
                return;
            } else {
                // Sign in failed
                if (response == null) {
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    return;
                }

                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    return;
                }
            }

            //showSnackbar(R.string.unknown_sign_in_response);
        }
    }
    private void setupPlaceAutoComplete() {
        final PlaceAutocompleteFragment places = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        PlaceSelectionListener placeSelectionListener = new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                LatLng latLng = place.getLatLng();
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));

            }

            @Override
            public void onError(Status status) {

            }
        };

        places.setOnPlaceSelectedListener(placeSelectionListener);

        //
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("VN")
                .build();
        places.setFilter(typeFilter);
    }

    private void showAlert(final int i) {
        String message, title, btnText;
        if (i == 1) {
            message = "Your Location Settings is set to 'Off'.\nPlease Enable to use this app";
            title = "Enable Location";
            btnText = "Location Settings";
        } else {
            message = "Please allow this app to access location service";
            title = "Permission access";
            btnText = "Grant";
        }

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton(btnText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (i == 1) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(PERMISSIONS, PERMISSION_ALL);
                            }
                        }
                    }
                })
                .setNegativeButton("Cancle", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        dialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location!=null){
            mCurrentLocation = location;
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        }
    }



    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //Lay vi tri hien tai
            Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            getCurrentLocation();

            if (isGeofenceClick == true)
                reloadMapMarkers();
        }

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(final LatLng latLng) {
                if (!client.isConnected()) {
                    Toast.makeText(getApplication(), getString(R.string.not_connected), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isGeofenceClick == true || (menuFragment!=null && menuFragment.isClickGeoFence() == true))
                {
                    //Tao geofence marker tren ban do
                    createGeofenceMarker(latLng);
                }

            }
        });

        mMap.setOnInfoWindowLongClickListener(new GoogleMap.OnInfoWindowLongClickListener() {
            @Override
            public void onInfoWindowLongClick(Marker marker) {
                if(isGeoMarker(marker.getPosition())) {
                    final String requestId = marker.getTitle().split(":")[1];
                    if (!client.isConnected()) {
                        Toast.makeText(HomeActivity.this, getString(R.string.not_connected), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    try {
                        List<String> idList = new ArrayList<>();
                        idList.add(requestId);
                        LocationServices.GeofencingApi.removeGeofences(client, idList).setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(@NonNull Status status) {
                                if (status.isSuccess()) {
                                    GeofenceStorage.removeGeofence(requestId);
                                    reloadMapMarkers();
                                } else {
                                    // Get the status code for the error and log it using a user-friendly message.
                                    String errorMessage = GeofenceTransitionsIntentService.getErrorString(HomeActivity.this,
                                            status.getStatusCode());
                                }
                            }
                        });
                    } catch (SecurityException securityException) {
                        logSecurityException(securityException);
                    }
                }
            }
        });
    }

    private void createGeofenceMarker(final LatLng latLng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(getResources().getString(R.string.geofence_title));

        LayoutInflater layoutInflater = LayoutInflater.from(getBaseContext());
        View mView = layoutInflater.inflate(R.layout.geofence_content_input, null);

        final EditText input = (EditText) mView.findViewById(R.id.geofence_input);

        builder.setView(mView);

        // Set up the buttons
        builder
                .setCancelable(false)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        final String key = getNewGeofenceNumber() + "";
                        final long expTime = System.currentTimeMillis() + GEOFENCE_EXPIRATION_IN_MILLISECONDS;
                        final String snippet=input.getText().toString();
                        addGeoMarker(key, latLng, snippet);
                        Geofence geofence = new Geofence.Builder()
                                .setRequestId(key)
                                .setCircularRegion(
                                        latLng.latitude,
                                        latLng.longitude,
                                        GEOFENCE_RADIUS_IN_METERS
                                )
                                .setExpirationDuration(GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                                .build();
                        try {
                            LocationServices.GeofencingApi.addGeofences(
                                    client,
                                    getGeofencingRequest(geofence),
                                    getGeofencePendingIntent()
                            ).setResultCallback(new ResultCallback<Status>() {
                                @Override
                                public void onResult(@NonNull Status status) {
                                    if (status.isSuccess()) {
                                        GeofenceStorage.saveToDb(key, latLng, expTime,snippet);
                                        Toast.makeText(HomeActivity.this, "Geofence Added", Toast.LENGTH_SHORT).show();
                                    } else {
                                        String errorMessage = GeofenceTransitionsIntentService.getErrorString(HomeActivity.this, status.getStatusCode());
                                        Log.e(TAG, errorMessage);
                                    }
                                }
                            });
                        } catch (SecurityException securityException) {
                            logSecurityException(securityException);
                        }
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        builder.show();
    }

    private GeofencingRequest getGeofencingRequest(Geofence geofence) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofence(geofence);
        return builder.build();
    }

    @SuppressWarnings({"MissingPermission"})
    private void makeRequestLocation() {
        Criteria criteria = new Criteria(); //Quan ly cac tieu chuan khi lua chon location provider
        criteria.setAccuracy(Criteria.ACCURACY_FINE);   //indicate a finer location accuracy requirement
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        provider = mLocationManager.getBestProvider(criteria, true);
    }


    private boolean isPermissionGranted() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permission is granted");
            return true;
        } else {
            Log.v(TAG, "Permission is not granted");
            return false;
        }
    }

    public boolean isLocationEnabled() {
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_gps:
                getCurrentLocation();
                break;
            case R.id.btn_menu_expand:
                showExpandedMenu();
                break;
            case R.id.btn_list_items:
                mDrawerLayout.openDrawer(Gravity.END);
                break;
            case R.id.btn_hamburger_menu:
                mDrawerLayout.openDrawer(GravityCompat.START);
                break;
        }
    }

    private void showExpandedMenu() {
        android.app.FragmentManager fm = getFragmentManager();
        menuFragment = MenuFragment.newInstance(this, mLocationManager, mMap);
        menuFragment.show(fm,"dialog");
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode)
        {
            case PERMISSION_ALL:
            {
                if (grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    makeRequestLocation();
                    Log.d(TAG, "permission was granted");
                }
                else
                {
                    Log.d(TAG, "permission was denied");
                }
                break;
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(100);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, (com.google.android.gms.location.LocationListener) this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private PendingIntent getGeofencePendingIntent() {
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void reloadMapMarkers() {
        mMap.clear();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        try (Cursor cursor = GeofenceStorage.getCursor()) {
            while (cursor.moveToNext()) {
                long expires = Long.parseLong(cursor.getString(cursor.getColumnIndex(GeofenceContract.GeofenceEntry.COLUMN_NAME_EXPIRES)));
                if(System.currentTimeMillis() < expires) {
                    String key = cursor.getString(cursor.getColumnIndex(GeofenceContract.GeofenceEntry.COLUMN_NAME_KEY));
                    double lat = Double.parseDouble(cursor.getString(cursor.getColumnIndex(GeofenceContract.GeofenceEntry.COLUMN_NAME_LAT)));
                    double lng = Double.parseDouble(cursor.getString(cursor.getColumnIndex(GeofenceContract.GeofenceEntry.COLUMN_NAME_LNG)));
                    String snippet=cursor.getString(cursor.getColumnIndex(GeofenceContract.GeofenceEntry.COLUMN_NAME_SNIPPET));
                    addGeoMarker(key, new LatLng(lat, lng), snippet);
                }
            }
        }
    }

    boolean doubleClickBack = false;

    @Override
    public void onBackPressed() {
        if (doubleClickBack) {
            super.onBackPressed();
            return;
        }

        this.doubleClickBack = true;
        mMap.clear();

        if (menuFragment != null) {
            menuFragment.setStop();
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleClickBack = false;
            }
        }, 2000);
    }

    boolean isGeoMarker(LatLng latLng)
    {
        for(int i =0;i<mGeofenceList.size();i++)
        {
            if(mGeofenceList.get(i).latitude==latLng.latitude && mGeofenceList.get(i).longitude==latLng.longitude)
                return true;
        }
        return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean("isGeofenceClick", isGeofenceClick);
    }

    public void setGeofenceClick(Boolean geofenceClick) {
        isGeofenceClick = geofenceClick;
    }

    private void setupFirstTimeUse()
    {
        final DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("users")
                .child(mAuth.getCurrentUser().getUid());
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                {
                    Log.v(TAG, "Da ton tai tai khoan. Khong khoi tao gia tri ban dau");
                }
                else
                {
                    FirebaseDatabase.getInstance().getReference("users").child(mAuth.getCurrentUser().getUid())
                            .child("email").setValue(mAuth.getCurrentUser().getEmail());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @SuppressLint("MissingPermission")
    public void getCurrentLocation() {
        if (isPermissionGranted() == false) {
            requestPermissions(PERMISSIONS, PERMISSION_ALL);
        }
        else if (provider!=null){
            Location location = mLocationManager.getLastKnownLocation(provider);
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        }
    }
}
