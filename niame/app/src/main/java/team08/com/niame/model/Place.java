package team08.com.niame.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by CONG_CANH on 11/27/2017.
 */

public class Place implements Parcelable {
    protected String placeID;
    protected String name; //ten place
    protected String address; //dia chi
    protected String open_time; //thoi gian mo cua
    protected double lat, lng;
    protected float rating;
    protected int numberReviews;

    public Place() {

    }

    public Place(String name, String address, String open_time, double lat, double lng, float rating, int numberReviews) {
        this.name = name;
        this.address = address;
        this.open_time = open_time;
        this.lat = lat;
        this.lng = lng;
        this.rating = rating;
        this.numberReviews = numberReviews;
    }

    protected Place(Parcel in) {
        placeID = in.readString();
        name = in.readString();
        address = in.readString();
        open_time = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        rating = in.readFloat();
        numberReviews = in.readInt();
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(placeID);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(open_time);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeFloat(rating);
        dest.writeInt(numberReviews);
    }


    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOpen_time() {
        return open_time;
    }

    public void setOpen_time(String open_time) {
        this.open_time = open_time;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getNumberReviews() {
        return numberReviews;
    }

    public void setNumberReviews(int numberReviews) {
        this.numberReviews = numberReviews;
    }
}

