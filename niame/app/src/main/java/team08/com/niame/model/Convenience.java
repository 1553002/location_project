package team08.com.niame.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by CONG_CANH on 12/8/2017.
 */

public class Convenience extends Place implements Parcelable {
    private String phone;
    private String website;

    public Convenience() {
    }

    public Convenience(String name, String address, String open_time, double lat, double lng, float rating, int numberReviews) {
        super(name, address, open_time, lat, lng, rating, numberReviews);
    }

    public Convenience(String name, String address, String open_time, double lat, double lng, float rating, int numberReviews, String phone, String website) {
        super(name, address, open_time, lat, lng, rating, numberReviews);
        this.phone = phone;
        this.website = website;
    }

    protected Convenience(Parcel in) {
        super(in);
        phone= in.readString();
        website = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(phone);
        dest.writeString(website);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Convenience> CREATOR = new Creator<Convenience>() {
        @Override
        public Convenience createFromParcel(Parcel in) {
            return new Convenience(in);
        }

        @Override
        public Convenience[] newArray(int size) {
            return new Convenience[size];
        }
    };


    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public static Creator<Convenience> getCREATOR() {
        return CREATOR;
    }
}
