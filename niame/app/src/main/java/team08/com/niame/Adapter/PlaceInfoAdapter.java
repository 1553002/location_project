package team08.com.niame.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import team08.com.niame.R;
import team08.com.niame.model.InfoItem;

/**
 * Created by CONG_CANH on 12/10/2017.
 */

public class PlaceInfoAdapter extends RecyclerView.Adapter<PlaceInfoAdapter.CustomViewHolder> {
    private List<InfoItem> infoItemList;
    private Context mContext;
    private OnItemClickListener onItemClickListener;

    public PlaceInfoAdapter( Context mContext, List<InfoItem> infoItemList) {
        this.infoItemList = infoItemList;
        this.mContext = mContext;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Get view cua list_row
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_info_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PlaceInfoAdapter.CustomViewHolder holder, int position) {
        final InfoItem infoItem = infoItemList.get(position);

        holder.icon.setImageResource(infoItem.getItem_icon());
        holder.textView.setText(infoItem.getContent());

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(infoItem);
            }
        };

        holder.textView.setOnClickListener((View.OnClickListener) listener);
    }

    @Override
    public int getItemCount() {
        return (null!=infoItemList ? infoItemList.size():0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView textView;

        public CustomViewHolder(View itemView) {
            super(itemView);
            this.icon = (ImageView) itemView.findViewById(R.id.icon);   //THiet lap icon ben place_info_item.xml
            this.textView = (TextView)itemView.findViewById(R.id.title);
        }
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(InfoItem item);
    }
}
