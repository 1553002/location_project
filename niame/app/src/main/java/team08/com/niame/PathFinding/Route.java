package team08.com.niame.PathFinding;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Administrator on 11/10/2017.
 */

public class Route {
    public Distance distance;
    public Estitime estitime;
    public String endAddress;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;

    public List<LatLng> points;
}
