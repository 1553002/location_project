package team08.com.niame.model;

import android.media.Image;
import android.widget.ImageView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import team08.com.niame.R;

/**
 * Created by CONG_CANH on 12/10/2017.
 */

public class InfoItem {
    private String content;
    private Integer item_icon;
    private List<Integer> list_item_icons = Arrays.asList(R.drawable.ic_place_location,R.drawable.ic_time_open,
            R.drawable.ic_phone,R.drawable.ic_website,R.drawable.ic_money,R.drawable.ic_note);

    public InfoItem() {
    }

    public InfoItem(String content, Integer item_icon) {
        this.content = content;
        this.item_icon = item_icon;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getItem_icon() {
        return item_icon;
    }

    public void setItem_icon(Integer item_icon) {
        this.item_icon = item_icon;
    }
}
