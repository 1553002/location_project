package team08.com.niame.Geofence;

/**
 * Created by Administrator on 11/28/2017.
 */

import android.app.Application;

public class App extends Application {

    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }
}
