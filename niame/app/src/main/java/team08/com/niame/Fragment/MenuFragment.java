package team08.com.niame.Fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import team08.com.niame.Activity.HomeActivity;
import team08.com.niame.R;
import team08.com.niame.place.GetPlaceFromFirebase;

/**
 * Created by CONG_CANH on 12/7/2017.
 */

public class MenuFragment extends DialogFragment implements View.OnClickListener, GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraMoveListener,
        GoogleMap.OnCameraMoveCanceledListener,
        GoogleMap.OnCameraIdleListener {

    GoogleMap mMap;
    LocationManager mLocationManager;
    Context mContext;
    GetPlaceFromFirebase nearbyPlaceData;

    HomeActivity homeActivity;

    String[] categories = {"convenience","carpark"};
    String category = null;
    LatLng latLng = null;

    private boolean clickGeoFence = false;
    boolean stop;

    public void setLocationManager(LocationManager mLocationManager) {
        this.mLocationManager = mLocationManager;
    }

    private FloatingActionButton button_car_park;
    private FloatingActionButton button_convenience;
    private FloatingActionButton button_atm;

    public MenuFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    @SuppressLint("ValidFragment")
    public MenuFragment(GoogleMap mMap, LocationManager mLocationManager, Context mContext) {
        this.mMap = mMap;
        this.mLocationManager = mLocationManager;
        this.mContext = mContext;

        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveCanceledListener(this);

        nearbyPlaceData = new GetPlaceFromFirebase(mMap, mContext );
    }

    public static MenuFragment newInstance(Context context, LocationManager locationManager, GoogleMap map) {
        MenuFragment frag = new MenuFragment(map,locationManager,context);
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        homeActivity = (HomeActivity)getActivity();
        stop = false;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        return inflater.inflate(R.layout.fab_layout, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Khoi tao cac button
        button_car_park = (FloatingActionButton) view.findViewById(R.id.fab_car_park);
        button_car_park.setOnClickListener(this);

        button_convenience = (FloatingActionButton) view.findViewById(R.id.fab_convenience);
        button_convenience.setOnClickListener(this);

        button_atm = (FloatingActionButton) view.findViewById(R.id.fab_atm);
        button_atm.setOnClickListener(this);
    }

    public final boolean isInternetOn() {
        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getActivity().getSystemService(mContext.CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            Toast.makeText(mContext, "Kiểm tra kết nối mạng!", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }
/*
        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.fab), message, Snackbar.LENGTH_LONG);*/

        /*View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();*/
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    public void setmMap(GoogleMap mMap) {
        this.mMap = mMap;
    }

    static boolean userGesture = false;

    @Override
    public void onCameraIdle() {
        latLng = mMap.getCameraPosition().target;
        if (userGesture == true && stop == false && category != null)
        {
            Location location = new Location(LocationManager.GPS_PROVIDER);
            location.setLatitude(latLng.latitude);
            location.setLongitude(latLng.longitude);

            double radius = 500;  //meter
            mMap.clear();   //

            nearbyPlaceData.findNearbyPlace(location, category, radius, false);
            userGesture = false;
        }
    }

    @Override
    public void onCameraMoveCanceled() {

    }

    @Override
    public void onCameraMove() {
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (stop == true)
            return;
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
            userGesture = true;
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_API_ANIMATION) {
        } else if (reason == GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION) {
        }
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        FloatingActionButton btn_More = (FloatingActionButton)homeActivity.findViewById(R.id.btn_list_items);

        switch (id)
        {
            case R.id.fab_car_park:
                category = categories[1];
                homeActivity.setGeofenceClick(false);
                break;
            case R.id.fab_convenience:
                category = categories[0];
                homeActivity.setGeofenceClick(false);
                break;
            case R.id.fab_atm:
                homeActivity.setGeofenceClick(true);
                homeActivity.reloadMapMarkers();
                btn_More.setVisibility(View.INVISIBLE);
                dismiss();
                return;
        }

        if (isInternetOn() == false)
            return;


        btn_More.setVisibility(View.VISIBLE);

        dismiss();

        @SuppressLint("MissingPermission")
        Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        double radius = 500;  //meter
        mMap.clear();   //

        nearbyPlaceData.findNearbyPlace(location,category, radius, true);
    }

    public void setStop()
    {
        stop = true;
    }

    public boolean isClickGeoFence() {
        return clickGeoFence;
    }
}
