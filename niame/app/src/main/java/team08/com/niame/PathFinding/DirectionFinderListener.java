package team08.com.niame.PathFinding;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Nhan 1553020 on 11/10/2017.
 */
public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
