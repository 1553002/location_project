package team08.com.niame.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by CONG_CANH on 11/27/2017.
 */

public class CarPark extends Place implements Parcelable{
    private String price; //Gia bai do
    private String notes;

    public CarPark(){}

    public CarPark(String name, String address, String open_time, double lat, double lng, float rating, int numberReviews) {
        super(name, address, open_time, lat, lng, rating, numberReviews);
    }

    public CarPark(String name, String address, String open_time, double lat, double lng, float rating, int numberReviews, String price, String notes) {
        super(name, address, open_time, lat, lng, rating, numberReviews);
        this.price = price;
        this.notes = notes;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    protected CarPark(Parcel in) {
        super(in);
        price = in.readString();
        notes = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(price);
        dest.writeString(notes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CarPark> CREATOR = new Creator<CarPark>() {
        @Override
        public CarPark createFromParcel(Parcel in) {
            return new CarPark(in);
        }

        @Override
        public CarPark[] newArray(int size) {
            return new CarPark[size];
        }
    };

    public String getPrice() {
        return price;
    }

    public String getNotes() {
        return notes;
    }

}
