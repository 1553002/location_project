package team08.com.niame.model;

import java.util.Date;

/**
 * Created by CongCanh on 12/24/2017.
 */

public class PlaceComment {
    String userEmail;
    String commentDate; //ngay comment
    String content;

    public PlaceComment(String userEmail, String commentDate, String content, int rating) {
        this.userEmail = userEmail;
        this.commentDate = commentDate;
        this.content = content;
        this.rating = rating;
    }

    int rating; //Muc rating dia diem

    public PlaceComment() {
    }

    public PlaceComment( String userEmail, String commentDate, int rating) {
        this.userEmail = userEmail;
        this.commentDate = commentDate;
        this.rating = rating;
    }


    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }


    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
