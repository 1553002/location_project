package team08.com.niame.place;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import team08.com.niame.Activity.PlaceInfoActivity;
import team08.com.niame.Adapter.NavigationDrawerAdapter;
import team08.com.niame.R;
import team08.com.niame.model.CarPark;
import team08.com.niame.model.Convenience;
import team08.com.niame.model.Place;

/**
 * Created by CONG_CANH on 11/26/2017.
 */

public class GetNearbyPlaceData
{
    private GoogleMap myMap;
    private Context context;

    public GetNearbyPlaceData (GoogleMap map, Context mContext){
        context = mContext;
        myMap = map;
    }


    public void findNearbyPlace(final Location curPos,final String category, final double radius){
        final ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage("Đang tìm kiếm");
        mProgressDialog.show();
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        final List<Place> places = new ArrayList<>();
        final HashMap<String, Place> list = new HashMap<>();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("places").child(category);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    Place place = null;
                    int icon = 0;

                    if (category.equals("carpark")) {
                        CarPark iCarpark = child.getValue(CarPark.class);
                        place = iCarpark;
                        icon = R.drawable.ic_carpark_location;
                    }
                    else if (category.equals("convenience")){
                        Convenience convenience = child.getValue(Convenience.class);
                        place = convenience;
                        icon = R.drawable.ic_convenience_location;
                    }

                    //Set icon id for place
                    place.setIconID(icon);

                    double l_latitude = place.getLat();
                    double l_longitude = place.getLng();

                    Location tmpLatLng = new Location("point B");
                    tmpLatLng.setLatitude(l_latitude);
                    tmpLatLng.setLongitude(l_longitude);

                    float distance = curPos.distanceTo(tmpLatLng);

                    if (distance <= radius)//EPI
                    {
                        Marker marker = myMap.addMarker(new MarkerOptions().position(new LatLng(l_latitude,l_longitude))
                                .title(place.getName())
                                .snippet(place.getAddress())
                                .icon(BitmapDescriptorFactory.fromResource(icon)));

                        //Them vao danh sach list marker, su dung cho click vao marker activity
                        list.put(marker.getId(),place);

                        place.distinceToCurrent = distance;

                        //Them vao danh sach places su dung cho recycle view
                        places.add(place);

                        Collections.sort(places, new Comparator<Place>() {
                            @Override
                            public int compare(Place o1, Place o2) {
                                return o1.distinceToCurrent > o2.distinceToCurrent? - 1 : (o1.distinceToCurrent <o2.distinceToCurrent) ? 1 : 0;
                            }
                        });

                    }
                }//end for

                //myMap.animateCamera(CameraUpdateFactory.zoomTo(15f), 1000, null);

                //1553002 - 14/Dec/2017
                //Set listen click on title
                myMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Place place = list.get(marker.getId());
                        if (place != null) {
                            Bundle b = new Bundle();
                            b.putParcelable("place_extra", place);
                            Intent intent = new Intent(context, PlaceInfoActivity.class);
                            intent.putExtras(b);
                            context.startActivity(intent);
                        }
                    }
                });

                //Thiet lap recycleview cho drawer layout
                setupRecycleview(new NavigationDrawerAdapter(context, places));
                //Ẩn dialog processing
                mProgressDialog.hide();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    //link tham khao lay het sdt tu cac id
    // https://stackoverflow.com/questions/38965731/how-to-get-all-childs-data-in-firebase-database

    private void setupRecycleview(NavigationDrawerAdapter navigationDrawerAdapter)
    {
        View rootView = ((Activity)context).getWindow().getDecorView().findViewById(R.id.nav_drwr_fragment);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.drawerList);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(navigationDrawerAdapter);
    }

    //inside the runnable will be the logic that you want to run
    void showProgressDialog(final Context context, final Runnable runnable) {
        final ProgressDialog ringProgressDialog = ProgressDialog.show(context, "Title ...", "Info ...", true);
        //you usually don't want the user to stop the current process, and this will make sure of that
        ringProgressDialog.setCancelable(false);
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {

                runnable.run();
                //after he logic is done, close the progress dialog
                ringProgressDialog.dismiss();
            }
        });
        th.start();
    }
}
