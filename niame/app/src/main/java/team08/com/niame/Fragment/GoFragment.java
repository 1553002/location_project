package team08.com.niame.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.BinderThread;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import team08.com.niame.Adapter.PlacesAutoCompleteAdapter;
import team08.com.niame.R;

/**
 * Created by Administrator on 11/14/2017.
 */


public class GoFragment extends android.support.v4.app.Fragment{

    public interface CommunicationInterface
    {
        public void sendText(String depa, String dest);
    }

    CommunicationInterface callback;
    TextView time_estimate;
    TextView distance_estimate;
    TextView place_name;

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        callback = (CommunicationInterface)getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        LinearLayout view_layout = (LinearLayout) inflater.inflate(R.layout.go_fragment,container,false);

        time_estimate = (TextView)view_layout.findViewById(R.id.texttime);
        distance_estimate =(TextView)view_layout.findViewById(R.id.textdistance);
        place_name = (TextView)view_layout.findViewById(R.id.text_place_destination);

        return view_layout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }


}
